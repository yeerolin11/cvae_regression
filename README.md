# CVAE_regression

Using G4 dataset to train a CVAE Regression
* A pretrained f_model is trained already
![CVAE regression architecture](./imgs/CVAE_regression.png)

## train 

```
python train.py
```

## inference

```
python sample_.py
```