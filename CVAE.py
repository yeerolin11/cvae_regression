import torch
from torch import nn

class CVAE_Regression(nn.Module):
    def __init__(self, input_dim, category_dim, condition_dim, latent_dim):
        super(CVAE_Regression, self).__init__()
        self.input_dim = input_dim
        self.condition_dim = condition_dim
        self.latent_dim = latent_dim
        self.category_dim = category_dim

        # Encoder layers
        self.encoder = nn.Sequential(
            nn.Linear(input_dim + condition_dim, 512),
            nn.ReLU(),
            nn.Linear(512, 256),
            nn.ReLU(),
        )
        self.fc_mu = nn.Linear(256, latent_dim)
        self.fc_var = nn.Linear(256, latent_dim)

        # Decoder layers
        self.decoder = nn.Sequential(
            nn.Linear(latent_dim + condition_dim, 256),
            nn.ReLU(),
            nn.Linear(256, 512),
            nn.ReLU(),
            # nn.Linear(512, input_dim),
        )
        self.continous_branch = nn.Linear(512, input_dim-category_dim)
            # No sigmoid activation here, output is continuous
        self.category_branch = nn.Sequential(
            nn.Linear(512, category_dim),
            nn.Softmax()
            # output is category
            )

    def encode(self, x, condition):
        combined = torch.cat([x, condition], dim=1)
        h = self.encoder(combined)
        return self.fc_mu(h), self.fc_var(h)

    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return mu + eps * std

    def decode(self, z, condition):
        combined = torch.cat([z, condition], dim=1)
        h = self.decoder(combined)
        return self.continous_branch(h), self.category_branch(h)

    def forward(self, x, condition):
        mu, logvar = self.encode(x.view(-1, self.input_dim), condition)
        z = self.reparameterize(mu, logvar)
        continous_fea, category_fea = self.decode(z, condition)
        return continous_fea, category_fea, mu, logvar


