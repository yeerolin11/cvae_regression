import pickle, joblib
import numpy as np
import torch
from xgboost import XGBRegressor


def load_model(pkl_file):
    with open(pkl_file, "rb") as f:
        model = pickle.load(f)
    return model


def get_f_model(model_path):
    alloutput_filename = f"{model_path}"

    f_model = load_model(alloutput_filename)

    return f_model


def scale_feature(x, scaler_dict, scaled_cols, device):
    mean = {key: scaler_dict["mean"][key] for key in scaled_cols}
    std = {key: scaler_dict["std"][key] for key in scaled_cols}
    mean_values = mean.values()
    std_values = std.values()

    if x.shape[1] > len(mean):
        for cat_col in range(x.shape[1] - len(mean)):
            mean[f"category_{cat_col}"] = 0
            std[f"category_{cat_col}"] = 1

    assert x.shape[1] == len(
        mean
    ), "Length of keys of features and scaler dictionary are not the same"

    if torch.is_tensor(x):
        x_cp = x.clone()
        mean = torch.tensor(list(mean_values)).to(device)
        std = torch.tensor(list(std_values)).to(device)
    else:
        x_cp = x.copy()
        mean = np.array(list(mean_values))
        std = np.array(list(std_values))

    return (x_cp - mean) / std


def unscale_feature(x, scaler_dict, scaled_cols, device):
    mean = {key: scaler_dict["mean"][key] for key in scaled_cols}
    std = {key: scaler_dict["std"][key] for key in scaled_cols}
    if x.shape[1] > len(mean):
        for cat_col in range(x.shape[1] - len(mean)):
            mean[f"category_{cat_col}"] = 0
            std[f"category_{cat_col}"] = 1
    assert x.shape[1] == len(
        mean
    ), "Length of keys of features and scaler dictionary are not the same"

    mean_values = mean.values()
    std_values = std.values()

    if torch.is_tensor(x):
        x_cp = x.clone()
        mean = torch.tensor(list(mean_values)).to(device)
        std = torch.tensor(list(std_values)).to(device)
    else:
        x_cp = x.copy()
        mean = np.array(list(mean_values))
        std = np.array(list(std_values))

    return x_cp * std + mean


def get_f_model_pred(input, f_model, device):
    if torch.is_tensor(input):
        input_cp = input.clone()

        # print("input_cp shape", input_cp.shape)
        parallelpath_class = torch.argmax(input_cp[:, -3:], dim=1)
        parallelpath_tensor = torch.eye(3).to(device)
        parallelpath_tensor = parallelpath_tensor[parallelpath_class]
        # print("parallelpath_tensor shape", parallelpath_tensor.shape)

        input_for_f_model = torch.cat(
            [input_cp[:, :-3], parallelpath_tensor],
            1,
        )
        # print("input_for_f_model shape", input_for_f_model.shape)

        response = f_model.predict(input_for_f_model.detach().cpu().numpy())
        return torch.tensor(response, requires_grad=True)
    else:
        input_cp = input.copy()
        parallelpath_class = np.argmax(input_cp[:, -3:], axis=1)
        parallelpath_tensor = np.eye(3)
        parallelpath_tensor = parallelpath_tensor[parallelpath_class]
        response = f_model.predict(input_cp)
        return response
