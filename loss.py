import torch
import torch.nn

def loss_function(
    recon_x_con,
    recon_x_cat,
    origin_x,
    mu,
    logvar,
    origin_x_response,
    recon_x_response,
):
    # see Appendix B from VAE paper:
    # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
    # https://arxiv.org/abs/1312.6114
    # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    mse_loss = torch.nn.MSELoss(reduction="sum")
    cross_entropy = torch.nn.CrossEntropyLoss(reduction="sum")

    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())

    continous_fea_dim = recon_x_con.shape[1]
    CON_RECONSTRUCT_LOSS = mse_loss(recon_x_con, origin_x[:, :continous_fea_dim])
    CAT_RECONSTRUCT_LOSS = cross_entropy(recon_x_cat, origin_x[:, continous_fea_dim:])
    
    RESPONSE_LOSS_pprime = mse_loss(recon_x_response, origin_x_response)

    return (
        KLD,
        CON_RECONSTRUCT_LOSS,
        CAT_RECONSTRUCT_LOSS,
        RESPONSE_LOSS_pprime,
    )
