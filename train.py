# %%
import argparse
from pathlib import Path
import torch
from torch.utils.data import DataLoader, random_split
from torch.utils.tensorboard import SummaryWriter

import utils
from dataset import G4ForCVAE_Dataset
from CVAE import CVAE_Regression
from loss import loss_function


# %%


def train_one_epoch(model, is_train, total_loss, data_loader, args):
    for batch_idx, (data, _) in enumerate(data_loader):
        x, _ = (
            data.to(args.device),
            _.to(args.device),
        )

        if is_train:
            args.optimizer.zero_grad()

        # origin x (scaled, feed to CVAE)
        x_response = x[:, : args.response_size]
        x_feature = x[:, args.response_size :]

        # reconstructed x
        recon_x_con, recon_x_cat, mu, logvar = model(x_feature, x_response)
        recon_x = torch.cat([recon_x_con, recon_x_cat], dim=1)

        # unscaled origin x (feed to f_model)
        x_response_unscaled = utils.unscale_feature(
            x=x_response,
            scaler_dict=args.f_model_scaler,
            scaled_cols=args.label_cols,
            device=args.device,
        )
        x_feature_unscaled = utils.unscale_feature(
            x=x_feature,
            scaler_dict=args.f_model_scaler,
            scaled_cols=[
                i for i in args.data_feature_cols if i not in args.categorial_cols
            ],
            device=args.device,
        )

        # unscaled reconstructed x (feed to f_model)
        recon_x_feature_unscaled = utils.unscale_feature(
            x=recon_x,
            scaler_dict=args.f_model_scaler,
            scaled_cols=[
                i for i in args.data_feature_cols if i not in args.categorial_cols
            ],
            device=args.device,
        )

        recon_x_response_unscaled = utils.get_f_model_pred(
            recon_x_feature_unscaled, f_model=args.f_model_pprime, device=args.device
        ).to(args.device)
        recon_x_response_unscaled = recon_x_response_unscaled.unsqueeze(0).t()
        recon_x_response = utils.scale_feature(
            x=recon_x_response_unscaled,
            scaler_dict=args.f_model_scaler,
            scaled_cols=args.label_cols,
            device=args.device,
        ).to(torch.float32)
        if is_train and batch_idx % 20 == 0 and (epoch + 1) % 10 == 0:
            print("-" * 20)
            print("x_response_unscaled", x_response_unscaled[0])
            print("recon_x_response_unscaled", recon_x_response_unscaled[0])
            print("*")
            print("x_feature", x_feature[0])
            print("recon_x", recon_x[0])
            print("*")
            print("x_response", x_response[0])
            print("recon_x_response", recon_x_response[0])

        """Compute loss"""
        (
            KL_loss,
            con_Resconstruct_loss,
            cat_Resconstruct_loss,
            Response_loss_pprime,
        ) = loss_function(
            recon_x_con,
            recon_x_cat,
            x_feature,
            mu,
            logvar,
            x_response,
            recon_x_response,
        )

        loss = (
            KL_loss
            + con_Resconstruct_loss
            + cat_Resconstruct_loss
            + Response_loss_pprime
        )

        if is_train:
            loss.backward()
            args.optimizer.step()
            if batch_idx % 20 == 0:
                print(
                    "Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}".format(
                        args.epochs,
                        batch_idx * len(data),
                        len(data_loader.dataset),
                        100.0 * batch_idx / len(data_loader),
                        loss.item() / len(data),
                    )
                )

        total_loss += loss.detach().cpu().numpy()

    return {
        "total_loss": total_loss,
        "loss": loss,
        "KL_loss": KL_loss,
        "con_Resconstruct_loss": con_Resconstruct_loss,
        "cat_Resconstruct_loss": cat_Resconstruct_loss,
        "Response_loss_pprime": Response_loss_pprime,
    }


def train(model, is_train, data_loader, args):
    if is_train:
        model.train()
    else:
        model.eval()

    total_loss = 0

    if is_train:
        loss_dict = train_one_epoch(model, is_train, total_loss, data_loader, args)
        print(
            "====> Epoch: {} Average loss: {:.4f}".format(
                epoch, loss_dict["total_loss"] / len(data_loader.dataset)
            )
        )
    else:
        with torch.no_grad():
            loss_dict = train_one_epoch(model, is_train, total_loss, data_loader, args)
        print(
            "====> Test set loss: {:.4f}".format(
                loss_dict["total_loss"] / len(data_loader.dataset)
            )
        )

    return loss_dict


def get_args_parser(add_help=True):
    label_cols = ["p_prime"]  # , "SystemEfficiency", "ArmatureConductor"
    feature_cols = [
        "ShaftSpeed",
        "PeakCurrent",
        "Stator_Lam_Dia",
        "Stator_Lam_Length",
        "Magnet_Length",
        "Rotor_Lam_Length",
        "MagTurnsConductor",
        "Wire_Diameter",
        "NumberStrandsHand",
        "Fin_Extension",
        "ParallelPaths",
    ]
    categorial_cols = ["ParallelPaths"]

    parser = argparse.ArgumentParser(description="CVAE G4 Experiment")
    parser.add_argument(
        "--experiment-title",
        type=str,
        default="cvae_experiment17",
        help="path stored dataset",
    )
    parser.add_argument(
        "--dataset-path",
        type=list,
        default=[
            "./data/G4_history_rawdata.csv",
            "./data/fake_from_pprime_xgbregressor_catpp.csv",
        ],
        help="path stored dataset",
    )
    parser.add_argument(
        "--surrogate-model-path",
        type=str,
        default="./models/f_model",
        help="f_model pickle file",
    )
    parser.add_argument(
        "--surrogate-model-scaler-path",
        type=str,
        default="./models/f_model/scaler_xgbregressor_catpp.pkl",
        help="f_model's scaler pickle file",
    )
    parser.add_argument(
        "--label_cols",
        type=list,
        default=label_cols,
        help="column types are belongs to label",
    )
    parser.add_argument(
        "--data_feature_cols",
        type=list,
        default=feature_cols,
        help="column types are belongs to feature",
    )
    parser.add_argument(
        "--categorial_cols",
        type=list,
        default=categorial_cols,
        help="column types are category",
    )
    parser.add_argument(
        "--drop-na",
        type=str,
        default="row",
        help="drop nan data by column or row (default: col)",
    )
    parser.add_argument(
        "--scaler_path",
        type=str,
        default="./models/scaler.pkl",
        help="path to store scaler",
    )
    parser.add_argument(
        "--output-path", type=str, default="./models/CVAE", help="path stored model"
    )
    parser.add_argument(
        "--train_ratio",
        type=float,
        default=0.8,
        metavar="N",
        help="ratio of training set",
    )
    parser.add_argument(
        "--batch-size",
        type=int,
        default=256,
        metavar="N",
        help="input batch size for training (default: 256)",
    )
    parser.add_argument(
        "--epochs",
        type=int,
        default=10000,
        metavar="N",
        help="number of epochs to train (default: 1000)",
    )
    parser.add_argument(
        "--learning-rate",
        type=float,
        default=1e-3,
        metavar="N",
        help="learning rate (default: 1e-4)",
    )
    parser.add_argument(
        "--intermediate-size",
        type=int,
        default=256,
        metavar="N",
        help="intermediate vector size (default: 12)",
    )
    parser.add_argument(
        "--latent-size",
        type=int,
        default=128,
        metavar="N",
        help="latent vector z size (default: 10)",
    )
    parser.add_argument(
        "--parallelpath-size",
        type=int,
        default=3,
        metavar="N",
        help="parallelpath size (default: 3)",
    )
    parser.add_argument(
        "--weight-reconstruct-loss",
        type=float,
        default=0.3,
        metavar="N",
        help="weight of reconstruct loss (default: 0.3)",
    )
    parser.add_argument(
        "--weight-response-loss",
        type=float,
        default=1,
        metavar="N",
        help="weight of response loss (default: 1)",
    )
    parser.add_argument(
        "--no-cuda", action="store_true", default=False, help="disables CUDA training"
    )
    parser.add_argument(
        "--seed", type=int, default=42, metavar="S", help="random seed (default: 1)"
    )
    parser.add_argument(
        "--log-interval",
        type=int,
        default=10,
        metavar="N",
        help="how many batches to wait before logging training status",
    )

    return parser


# %%
if __name__ == "__main__":
    args = get_args_parser().parse_args(args=[])

    writer = SummaryWriter(f"runs/{args.experiment_title}")

    args.cuda = not args.no_cuda and torch.cuda.is_available()

    torch.manual_seed(args.seed)

    if args.cuda:
        torch.cuda.set_device(4)
        args.device = torch.device("cuda")
    else:
        args.device = torch.device("cpu")

    dataset = G4ForCVAE_Dataset(
        datapath=args.dataset_path,
        feature_cols=args.data_feature_cols,
        label_cols=args.label_cols,
        categorial_cols=args.categorial_cols,
        drop_na=args.drop_na,
        scaled=True,
        scaler_path=args.scaler_path,
    )
    args.f_model_scaler = dataset.scaler

    train_size = int(dataset.__len__() * args.train_ratio)
    test_size = dataset.__len__() - train_size

    train_dataset, test_dataset = random_split(
        dataset=dataset,
        lengths=[train_size, test_size],
        generator=torch.Generator().manual_seed(args.seed),
    )

    train_loader = DataLoader(
        dataset=train_dataset, batch_size=args.batch_size, shuffle=True, num_workers=4
    )
    test_loader = DataLoader(
        dataset=test_dataset, batch_size=args.batch_size, shuffle=True, num_workers=4
    )

    args.response_size = len(args.label_cols)
    args.feature_size = len(dataset.feature_cols)
    weight_reconstruct_loss = args.weight_reconstruct_loss

    # args.f_model = utils.get_f_model(
    #     f"{args.surrogate_model_path}/alloutput_xgbregressor_v3.pkl"
    # )
    args.f_model_pprime = utils.get_f_model(
        f"{args.surrogate_model_path}/pprime_xgbregressor_catpp_notscaled.pkl"
    )
    # args.f_model_Atmp = utils.get_f_model(
    #     f"{args.surrogate_model_path}/Atmp_xgbregressor.pkl"
    # )

    model = CVAE_Regression(
        args.feature_size,
        len(dataset.categorial_cols),
        args.response_size,
        args.latent_size,
    ).to(args.device)

    # args.optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate)
    args.optimizer = torch.optim.SGD(model.parameters(), lr=1e-6, momentum=0.9)

    for epoch in range(1, args.epochs + 1):
        train_loss = train(model, is_train=True, data_loader=train_loader, args=args)
        test_loss = train(model, is_train=False, data_loader=test_loader, args=args)
        for loss_item in train_loss:
            writer.add_scalars(
                loss_item,
                {"train": train_loss[loss_item], "test": test_loss[loss_item]},
                epoch,
            )

    model_save_path = (
        Path(args.output_path) / f"{args.experiment_title}-epoch{args.epochs}.pt"
    )
    torch.save(model, model_save_path)

    import json

    with open(f"./config/{args.experiment_title}.json", "wt") as f:
        try:
            json.dump(vars(args), f, indent=4)
        except TypeError:
            pass
