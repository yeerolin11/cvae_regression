# %%
import os
import torch
from torch.utils.data import Dataset, DataLoader
from torch import nn, optim
from torch.nn import functional as F
from torchvision import datasets, transforms

from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, r2_score
from xgboost import XGBRegressor

import pandas as pd
import numpy as np
import joblib
import utils
from CVAE import CVAE_BN, CVAE_Regression

# os.environ["CUDA_VISIBLE_DEVICES"] = "4"
torch.cuda.set_device(1)
device = torch.device("cuda")
kwargs = {"num_workers": 1, "pin_memory": True}


def main():
    ROOT = "."
    MODEL_PATH = "models"
    MODEL_pkl = "/nvme6n1/CVAE_G4/models/CVAE/cvae_experiment16-epoch10000.pt"

    # model_save_path = f"{ROOT}/{MODEL_PATH}/CVAE/{MODEL_pkl}"
    model_save_path = MODEL_pkl
    # model = CVAE_BN(feature_size, intermediate_dim, latent_size, response_size).to(device)
    model = torch.load(model_save_path)

    MODEL_PATH = "models"
    alloutput_filename = f"{ROOT}/{MODEL_PATH}/f_model/alloutput_xgbregressor_v3.pkl"
    pprime_filename = (
        f"{ROOT}/{MODEL_PATH}/f_model/pprime_xgbregressor_catpp_notscaled.pkl"
    )

    f_model = utils.load_model(alloutput_filename)
    f_model_pprime = utils.load_model(pprime_filename)
    f_model_scaler = utils.load_model(
        f"{ROOT}/{MODEL_PATH}/f_model/scaler_xgbregressor_catpp.pkl"
    )
    print(f_model_scaler)

    """---
    # Sampling
    """
    feature_cols = [
        "ShaftSpeed",
        "PeakCurrent",
        "Stator_Lam_Dia",
        "Stator_Lam_Length",
        "Magnet_Length",
        "Rotor_Lam_Length",
        "MagTurnsConductor",
        "Wire_Diameter",
        "NumberStrandsHand",
        "Fin_Extension",
        "ParallelPaths",
    ]

    feature_size = 16
    response_size = 3
    parallelpath_size = 3
    latent_size = 128

    sample_size = 1000
    target_response = [4000]
    response_scaler = {
        "mean": {
            "p_prime": 5804.474662907993,
        },
        "std": {
            "p_prime": 2775.1334990648847,
        },
    }
    feature_scaler = {
        "mean": {
            "ShaftSpeed": 4760.3144380372105,
            "PeakCurrent": 341.57717460382753,
            "Stator_Lam_Dia": 319.1192205681853,
            "Stator_Lam_Length": 307.3302828693406,
            "Magnet_Length": 310.54142958719495,
            "Rotor_Lam_Length": 307.0935183399154,
            "MagTurnsConductor": 32.20802959416821,
            "Wire_Diameter": 3.27427165123275,
            "NumberStrandsHand": 31.378196061364378,
            "Fin_Extension": 30.777651014351427,
            "ParallelPaths_0": 0.2361005331302361,
            "ParallelPaths_1": 0.555978674790556,
            "ParallelPaths_2": 0.2079207920792079,
        },
        "std": {
            "ShaftSpeed": 2155.5549922013456,
            "PeakCurrent": 277.0444137148512,
            "Stator_Lam_Dia": 291.3909043750181,
            "Stator_Lam_Length": 299.6797041985367,
            "Magnet_Length": 302.9169138356906,
            "Rotor_Lam_Length": 299.68087271028446,
            "MagTurnsConductor": 29.320498772187204,
            "Wire_Diameter": 2.899651275242674,
            "NumberStrandsHand": 30.2375099269472,
            "Fin_Extension": 30.526462096048874,
            "ParallelPaths_0": 0.42470777806306703,
            "ParallelPaths_1": 0.49688353813555186,
            "ParallelPaths_2": 0.40584191113967144,
        },
    }
    
    target_response_tensor = np.array([target_response] * sample_size, dtype="float32")
    scaled_target_response = utils.scale_feature(
        x=target_response_tensor.copy(), scaler_dict=response_scaler
    )
    scaled_target_response = torch.from_numpy(scaled_target_response).to(device)
    print(target_response_tensor)

    with torch.no_grad():
        sample = torch.randn(sample_size, latent_size).to(device)
        print("shape of sample: ", sample.shape)
        print("shape of scaled_target_response: ", scaled_target_response.shape)
        recon_sample = model.decode(sample, scaled_target_response)

        # reconx_to_fModel_input = utils.recon_x_to_f_input_catpp(
        #     origin=False,
        #     reconx=recon_sample,
        #     feature_col=feature_cols,
        #     pp_start=feature_size - parallelpath_size - response_size,
        #     scaler_dict=f_model_scaler,
        #     device=device,
        # )

        reconx_f_model_input = utils.unscale_feature(recon_sample, feature_scaler)
        
        parallelpath_class = np.argmax(reconx_f_model_input[:, -3:].detach().cpu().numpy(), axis=1)
        parallelpath_tensor = np.eye(parallelpath_size)[parallelpath_class]

        input_for_alloutput_model = np.concatenate(
            (reconx_f_model_input[:, :-3].detach().cpu().numpy(), parallelpath_tensor),
            axis=1,
        )

        reconx_response = utils.get_f_model_pred_v2(
            input_for_alloutput_model,
            f_model=f_model_pprime,
            f_model_scaler_mean={},
            f_model_scaler_std={},
        )
        # gt_response = f_model.predict(recon_x_to_alloutput_model.detach().cpu().numpy())
        # r2_recon_x = r2_score(target_response_tensor.cpu().numpy(), reconx_response)
    # print(gt_response)
    # print(r2_recon_x)
    # print(recon_x_to_alloutput_model)

    # inverse_x = f_model_scaler.inverse_transform(
    #     (recon_x_to_alloutput_model).cpu().numpy()
    # )
    closest_id = np.argmin(abs(reconx_response-target_response_tensor))
    print("closest_id: ", closest_id, " and p_prime=", reconx_response[closest_id].item())
    top_closest_indices = np.argsort(abs(reconx_response-target_response_tensor))
    
    for i, top_k in enumerate(top_closest_indices[0]):
        if i < 10:
            # pprime
            print(reconx_response[top_k].item())
    print("-")
    for param_id, param in enumerate(
            [
                "ShaftSpeed",
                "PeakCurrent",
                "Stator_Lam_Dia",
                "Stator_Lam_Length",
                "Magnet_Length",
                "Rotor_Lam_Length",
                "MagTurnsConductor",
                "Wire_Diameter",
                "NumberStrandsHand",
                "Fin_Extension",
                "ParallelPaths_1",
                "ParallelPaths_2",
                "ParallelPaths_4",
            ]
        ):
        # param, "=", 
            print(input_for_alloutput_model[closest_id, param_id].item())
        
    # for sample_id in range(reconx_response.shape[0]):
    #     # for param_id, param in enumerate(
    #     #     [

    #     #         "ShaftSpeed",
    #     #         "PeakCurrent",
    #     #         "Stator_Lam_Dia",
    #     #         "Stator_Lam_Length",
    #     #         "Magnet_Length",
    #     #         "Rotor_Lam_Length",
    #     #         "MagTurnsConductor",
    #     #         "Wire_Diameter",
    #     #         "NumberStrandsHand",
    #     #         "Fin_Extension",
    #     #         "ParallelPaths_1",
    #     #         "ParallelPaths_2",
    #     #         "ParallelPaths_4",
    #     #     ]
    #     # ):
    #     #     print(param, "=", recon_sample[sample_id, param_id])
    #     result = ""
    #     for param_id, param in enumerate(["p_prime"]):
    #         result += "p_prime"
    #         result += "="
    #         result += str(reconx_response[sample_id].item())
    #         result += " "
    #     print(result)
    print("--------")


if __name__ == "__main__":
    main()
