# %%
import numpy as np
import pandas as pd
from pathlib import Path

from sklearn.preprocessing import OneHotEncoder
from torch.utils.data import Dataset

import utils


class G4ForCVAE_Dataset(Dataset):
    def __init__(
        self,
        datapath,
        feature_cols,
        label_cols,
        categorial_cols=None,
        drop_na="row",
        scaled=True,
        scaler_path="./scaler.pkl",
    ):
        self.datapath = [f for f in datapath]
        self.feature_cols = []
        self.label_cols = label_cols
        self.categorial_cols = []
        self.drop_na = drop_na
        self.scaled = scaled
        self.scaler = None
        self.scaler_path = Path(scaler_path)

        self.Xdata_columns = list()
        self.data_df, self.X_data, self.y_data = self.prepareData(
            feature_cols, categorial_cols, label_cols
        )

    def prepareData(self, feature_cols, categorial_cols, label_cols):
        """Preprocess data.
        First, read data from csv file. Including remove nan cell and convert categorial columns into one-hot encoding.
        Second, use StandardScaler to transform the data.

        Returns:
            numpy.ndarray, numpy.ndarray: Transformed samples and labels
        """
        dataset = self.processRawdata(
            wanted_cols=label_cols + feature_cols,
            categorial_cols=categorial_cols,
            drop_na=self.drop_na,
        )

        if self.scaled:
            data_describe = dataset.describe()
            numeric_features = pd.Index(
                [
                    i
                    for i in self.feature_cols + self.label_cols
                    if i not in self.categorial_cols
                ]
            )
            dataset[numeric_features] = dataset[numeric_features].apply(
                lambda x: (x - x.mean()) / (x.std())
            )
            self.scaler = {"mean": {}, "std": {}}
            for i in numeric_features:
                self.scaler["mean"][i] = data_describe[i]["mean"]
                self.scaler["std"][i] = data_describe[i]["std"]

        labels = dataset[self.label_cols].to_numpy()
        samples = dataset.drop(columns=self.label_cols).to_numpy()

        return dataset, samples.astype(np.float32), labels.astype(np.float32)

    def processRawdata(self, wanted_cols, categorial_cols, drop_na):
        """Process rawdata. First, read csv file as dataframe. Second, remove NAN rows/cols.
            If there is categorial-type columns, convert to one-hot encoding. Last, char '[', ']' not allowed to exist.

        Args:
            wanted_cols (list): Columns used to training the model as features.
            drop_na (str): Drop Nan type. 'row' for droping any row if there is Nan cell; 'col' for droping col if Nan.

        Raises:
            ValueError: 'drop_na' is not 'row' or 'col'.

        Returns:
            pandas.DataFrame: Valid data for training.
        """
        dataset = pd.DataFrame()
        if len(self.datapath) >= 1:
            for csv_f in self.datapath:
                one_df = pd.read_csv(csv_f, usecols=wanted_cols)
                dataset = pd.concat([dataset, one_df])
        # First k columns are the labels column
        dataset = dataset[wanted_cols]

        # Remove NAN rows
        print(f"Dataset size before drop nan: {dataset.shape}.")
        dataset.isna().sum()
        if drop_na == "row":
            dataset = dataset.dropna()
            print("Removed NAN rows.")
        elif drop_na == "col":
            dataset = dataset.dropna(axis="columns")
            print("Removed NAN columns.")
        else:
            raise ValueError("Not defined argument 'drop_na' (row or col).")

        # Convert Categorial type to one-hot encoding
        if categorial_cols is not None:
            for col in categorial_cols:
                dataset[col] = dataset[col].astype("category")
                dataset[f"{col}_new"] = dataset[col].cat.codes

                # Create an instance of One-hot-encoder
                enc = OneHotEncoder()
                enc_data = pd.DataFrame(
                    enc.fit_transform(dataset[[f"{col}_new"]]).toarray()
                )
                enc_data.columns = [f"{col}_{i}" for i in list(enc_data.columns)]
                #
                self.categorial_cols += list(enc_data.columns)

                # Merge with main
                dataset = dataset.join(enc_data)
                dataset.drop(columns=[f"{col}_new", col], inplace=True)

        print("Get rid of '[', ']' in column name.")
        dataset.columns = dataset.columns.str.replace("[", "")
        dataset.columns = dataset.columns.str.replace("]", "")
        print(f"Dataset size: {dataset.shape}.")
        print(f"Columns of dataset: {list(dataset.columns)}")
        print(f"First {len(list(self.label_cols))} columns are labels column.")
        self.feature_cols = [
            i for i in list(dataset.columns) if i not in self.label_cols
        ]

        return dataset

    def __getitem__(self, index):
        return self.X_data[index], self.y_data[index]

    def __len__(self):
        return len(self.X_data)


# unittest
if __name__ == "__main__":
    # setting
    datapath = [
        "./data/G4_history_rawdata.csv",
        "./data/fake_from_pprime_xgbregressor_catpp.csv",
    ]
    label_cols = ["p_prime", "SystemEfficiency", "ArmatureConductor"]
    feature_cols = [
        "ShaftSpeed",
        "PeakCurrent",
        "Stator_Lam_Dia",
        "Stator_Lam_Length",
        "Magnet_Length",
        "Rotor_Lam_Length",
        "MagTurnsConductor",
        "Wire_Diameter",
        "NumberStrandsHand",
        "Fin_Extension",
        "ParallelPaths",
    ]
    categorial_cols = ["ParallelPaths"]
    drop_na = "row"
    f_model_path = f"./models/f_model/pprime_xgbregressor_catpp_notscaled.pkl"

    # load dataset
    data = G4ForCVAE_Dataset(
        datapath=datapath,
        feature_cols=feature_cols,
        label_cols=label_cols,
        categorial_cols=categorial_cols,
        drop_na=drop_na,
        scaled=True,
        scaler_path="./scaler_.pkl",
    )

    sample_features = data.X_data
    sample_response = data.y_data

    f_model_pprime = utils.load_model(f_model_path)

    # unscaled origin feature
    unscaled_response = utils.unscale_feature(
        x=sample_response,
        scaler_dict=data.scaler,
        scaled_cols=data.label_cols,
        device="cpu",
    )
    unscaled_feature = utils.unscale_feature(
        x=sample_features,
        scaler_dict=data.scaler,
        scaled_cols=[i for i in feature_cols if i not in categorial_cols],
        device="cpu",
    )

    # f_model can only predicted by unscaled features
    f_model_pred_response = utils.get_f_model_pred(
        unscaled_feature, f_model=f_model_pprime, device="cpu"
    )

# %%
